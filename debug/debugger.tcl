# Usage with Vitis IDE:
# In Vitis IDE create a Single Application Debug launch configuration,
# change the debug type to 'Attach to running target' and provide this 
# tcl script in 'Execute Script' option.
# Path of this script: /home/emanuel/Documentos/Vitis/workspace/hello_world_system/_ide/scripts/debugger_hello_world-default.tcl
# 
# 
# Usage with xsct:
# To debug using xsct, launch xsct and run below command
# source /home/emanuel/Documentos/Vitis/workspace/hello_world_system/_ide/scripts/debugger_hello_world-default.tcl
# 

connect -url tcp:127.0.0.1:3121
targets -set -filter {jtag_cable_name =~ "Digilent Arty A7-35T 210319AB5222A" && level==0 && jtag_device_ctx=="jsn-Arty A7-35T-210319AB5222A-0362d093-0"}


fpga -file ${path}/${name}/_ide/bitstream/${proj}wrapper.bit

#! fpga -file /home/emanuel/Documentos/Vitis/workspace/hello_world/_ide/bitstream/design_1_wrapper.bit


targets -set -nocase -filter {name =~ "*microblaze*#0" && bscan=="USER2" }

loadhw -hw ${path}/${wrapper}/export/${wrapper}/hw/${proj}wrapper.xsa -regs
#! loadhw -hw /home/emanuel/Documentos/Vitis/workspace/design_1_wrapper/export/design_1_wrapper/hw/design_1_wrapper.xsa -regs
configparams mdm-detect-bscan-mask 2
targets -set -nocase -filter {name =~ "*microblaze*#0" && bscan=="USER2" }
rst -system
after 3000
targets -set -nocase -filter {name =~ "*microblaze*#0" && bscan=="USER2" }



dow ${path}/${name}/Debug/${name}.elf

#! dow /home/emanuel/Documentos/Vitis/workspace/hello_world/Debug/hello_world.elf


targets -set -nocase -filter {name =~ "*microblaze*#0" && bscan=="USER2" }
con

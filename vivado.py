import os

# Project Variables
BLOCK_DESIGN = 'src/settings/blockdesign.tcl'
VIVADO_SETTINGS = 'src/settings/vivado.tcl'
VIVADO_PROJECT = os.getcwd()
VIVADO_PROJECT_NAME = VIVADO_PROJECT.split('/')[-1]
TOOLCHAIN_PATH = os.getenv('TOOLCHAIN_PATH')

VIVADO = 'vivado'
VIVADO_ARGS = ' -mode batch -nolog -nojournal -notrace -source ' + TOOLCHAIN_PATH + '/utils/vivado_scripts/vivado_project.tcl -q'

VIVADO_PROJECT_PATH = VIVADO_PROJECT + '/output'
VIVADO_BD_PATH = VIVADO_PROJECT_PATH + '/' + VIVADO_PROJECT_NAME + '.srcs/sources_1/bd/' + VIVADO_PROJECT_NAME + '_bd/' + VIVADO_PROJECT_NAME + '_bd.bd'
VIVADO_SYNT = VIVADO_PROJECT_PATH + '/' + VIVADO_PROJECT_NAME + '.runs/synth_1/' + VIVADO_PROJECT_NAME + '_bd_wrapper.dcp'
VIVADO_IMPL = VIVADO_PROJECT_PATH + '/' + VIVADO_PROJECT_NAME + '.runs/impl_1/' + VIVADO_PROJECT_NAME + '_bd_wrapper_routed.dcp'
VIVADO_HWDEF = VIVADO_PROJECT_PATH + '/' + VIVADO_PROJECT_NAME + '.runs/impl_1/' + VIVADO_PROJECT_NAME + '_bd_wrapper.hwdef'
VIVADO_SYSDEF = VIVADO_PROJECT_PATH + '/' + VIVADO_PROJECT_NAME + '.runs/impl_1/' + VIVADO_PROJECT_NAME + '_bd_wrapper.sysdef'
VIVADO_BIT = VIVADO_PROJECT_PATH + '/' + VIVADO_PROJECT_NAME + '.runs/impl_1/' + VIVADO_PROJECT_NAME + '_bd_wrapper.bit'
VIVADO_HW_XSA = VIVADO_PROJECT_PATH + '/hw/' + VIVADO_PROJECT_NAME + 'wrapper.xsa'


PRJ_CMD = VIVADO + VIVADO_ARGS + ' -tclargs ' + VIVADO_PROJECT + ' ' + VIVADO_PROJECT_NAME + ' create_prj no_create_bd no_synth no_impl_dsgn no_bitstream'
	
BD_CMD = VIVADO + VIVADO_ARGS + ' -tclargs ' + VIVADO_PROJECT + ' ' + VIVADO_PROJECT_NAME + ' open_prj create_bd no_synth no_impl_dsgn no_bitstream'
 
SYN_CMD = VIVADO + VIVADO_ARGS + ' -tclargs ' + VIVADO_PROJECT + ' ' + VIVADO_PROJECT_NAME + ' open_prj no_create_bd do_synth no_impl_dsgn no_bitstream'

IMP_CMD = VIVADO + VIVADO_ARGS + ' -tclargs ' + VIVADO_PROJECT + ' ' + VIVADO_PROJECT_NAME + ' open_prj no_create_bd no_synth do_impl_dsgn no_bitstream'

BIT_CMD = VIVADO + VIVADO_ARGS + ' -tclargs ' + VIVADO_PROJECT + ' ' + VIVADO_PROJECT_NAME + ' open_prj no_create_bd no_synth no_impl_dsgn do_bitstream'

XSA_CMD = VIVADO + VIVADO_ARGS + ' -tclargs ' + VIVADO_PROJECT + ' ' + VIVADO_PROJECT_NAME + ' open_prj no_create_bd no_synth no_impl_dsgn no_do_bitstream do_xsa'

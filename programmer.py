from vivado import *
from vitis_doit import APP_NAME

HOST = ' 127.0.0.1'
PORT = ' 3121'

VITIS = 'xsct'
VITIS_PROG_ARGS = ' -batch -source ' + TOOLCHAIN_PATH + '/utils/prog_scripts/prog.tcl'

FW_CMD = VITIS + VITIS_PROG_ARGS + ' ' + VIVADO_PROJECT + ' ' + VIVADO_PROJECT_NAME + HOST + PORT + ' ' + APP_NAME

PL_CMD = VITIS + VITIS_PROG_ARGS + ' ' + VIVADO_PROJECT + ' ' + VIVADO_PROJECT_NAME + HOST + PORT + ' pl' # TODO: VER ESTO



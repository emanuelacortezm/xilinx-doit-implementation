from distutils.debug import DEBUG
import os
from platform import platform
from vivado import *

VITIS_PATH = os.getenv('VITIS_PATH')

def clean_vitis():
    os.system('rm output/' + VITIS_PROJECT_NAME + '.sdk -rf')

VITIS_PROJECT = VIVADO_PROJECT
VITIS_PROJECT_NAME = VIVADO_PROJECT_NAME


#* Variables de ubicación de los archivos

DESIGN_NAME = 'design_test'
VITIS_WRAPPER_NAME ='design_test_wrapper'
VITIS_WORKSPACE = VIVADO_PROJECT_PATH + '/workspace'
VITIS_HW_XSA = 'hw/'+ VIVADO_PROJECT_NAME + 'wrapper.xsa'
SRC ='recursos'
DEBUG ='debug'

VITIS = 'xsct -eval "'
APP_NAME = 'hello_world'

## Archivos a verificar
PLATFORM = VIVADO_PROJECT + '/output/workspace/' + VITIS_WRAPPER_NAME + '/platform.spr'
WRAPPER = VITIS_WORKSPACE + '/' + APP_NAME + '/_ide/bitstream/' + VIVADO_PROJECT_NAME + 'wrapper.bit'
HW_V = VITIS_WORKSPACE + '/' + VITIS_WRAPPER_NAME + '/export/' + VITIS_WRAPPER_NAME + '/hw/' + VIVADO_PROJECT_NAME + 'wrapper.xsa'
ELF= VITIS_WORKSPACE + '/' + APP_NAME + '/Debug/' + APP_NAME + '.elf'


VITIS_WS = VITIS + 'setws ' + VITIS_WORKSPACE + '"'
VITIS_PLAT = VITIS + 'platform create -name {' + VITIS_WRAPPER_NAME +'} -hw {' + VIVADO_HW_XSA +'} -out {'+ VITIS_WORKSPACE + '} ; platform write ; domain create -name {freertos10_xilinx_microblaze_0} -display-name {freertos10_xilinx_microblaze_0} -os {freertos10_xilinx} -proc {microblaze_0} -runtime {cpp} -arch {32-bit} -support-app {freertos_hello_world} ; platform generate -domains ; platform active {' + VITIS_WRAPPER_NAME + '} ; platform generate -quick " ' 

VITIS_APP = VITIS +'setws ' + VITIS_WORKSPACE +'; app create -name '+ APP_NAME +' -hw '+ VITIS_WORKSPACE + '/'+ VITIS_WRAPPER_NAME +'/'+ VITIS_HW_XSA +' -proc microblaze_0 -os freertos10_xilinx "'

VITIS_IMP = VITIS + 'setws ' + VITIS_WORKSPACE +'; importsources -name '+ APP_NAME +' -path '+ VIVADO_PROJECT + '/src/' + SRC + ' -target-path /src ; importsources -name '+ APP_NAME +'_system/ -path '+ VIVADO_PROJECT + '/src/' + DEBUG + ' -target-path _ide/scripts "'

VITIS_BUILD = VITIS + 'setws ' + VITIS_WORKSPACE +'; app build -name '+ APP_NAME + '"'

VITIS_COMP  = 'updatemem -force -meminfo ' + VITIS_WORKSPACE + '/' + APP_NAME + '/_ide/bitstream/' + VITIS_WRAPPER_NAME +'.mmi -bit ' + VITIS_WORKSPACE + '/' + APP_NAME + '/_ide/bitstream/' + VITIS_WRAPPER_NAME + '.bit -data ' + VITIS_PATH + '/data/embeddedsw/lib/microblaze/mb_bootloop_le.elf -proc ' + VITIS_PROJECT_NAME + '_bd_i/microblaze_0 -out ' + VITIS_WORKSPACE + '/' + APP_NAME + '/_ide/bitstream/download.bit'

RUN_PROG    = VITIS + 'variable name; variable path; variable proj; variable wrapper; set name ' + APP_NAME + '; set proj ' + VIVADO_PROJECT_NAME + '; set wrapper '+ VITIS_WRAPPER_NAME +'; set path ' + VITIS_WORKSPACE + '; source ' + VITIS_WORKSPACE + '/' +  APP_NAME + '_system/_ide/scripts/debugger.tcl"'









##################################################################################################################################

#! Creación de la plataforma
# setws /home/emanuel/Documentos/Vitis/workspace/proyecto_1; platform create -name {design_1_wrapper} -hw {/home/emanuel/Documentos/PPS/arty/design_1_wrapper.xsa} -out {/home/emanuel/Documentos/Vitis/workspace}; platform write

#! Creación del dominio
# domain create -name {freertos10_xilinx_microblaze_0} -display-name {freertos10_xilinx_microblaze_0} -os {freertos10_xilinx} -proc {microblaze_0} -runtime {cpp} -arch {32-bit} -support-app {freertos_hello_world} \ platform generate -domains \ platform active {design_test_wrapper} \ platform generate -quick

#! Elección del workspace
# setws /home/emanuel/Documentos/Vitis/workspace/proyecto_1

#! Creación del proyecto de aplicación 
# app create -name test_xsct -hw /home/emanuel/Documentos/Vitis/workspace/design_1_wrapper_1/hw/design_1_wrapper.xsa -proc microblaze_0 -os freertos10_xilinx 

#! Programación de la placa
#updatemem -force -meminfo /home/emanuel/Documentos/Vitis/workspace/test/_ide/bitstream/design_1_wrapper.mmi -bit /home/emanuel/Documentos/Vitis/workspace/test/_ide/bitstream/design_1_wrapper.bit -data /home/emanuel/programas/Xilinx/Vitis/2021.1/data/embeddedsw/lib/microblaze/mb_bootloop_le.elf -proc design_1_i/microblaze_0 -out /home/emanuel/Documentos/Vitis/workspace/test/_ide/bitstream/download.bit

#! RUN DEBUG
#connect -url tcp:127.0.0.1:3121;
#targets -set -filter {jtag_cable_name =~ "Digilent Arty A7-35T 210319AB5222A" && level==0 && jtag_device_ctx=="jsn-Arty A7-35T-210319AB5222A-0362d093-0"};
#fpga -file /home/emanuel/Documentos/Vitis/workspace/hello_world/_ide/bitstream/download.bit;
#targets -set -nocase -filter {name =~ "*microblaze*#0" && bscan=="USER2" };
#loadhw -hw /home/emanuel/Documentos/Vitis/workspace/design_1_wrapper/export/design_1_wrapper/hw/design_1_wrapper.xsa -regs;
#configparams mdm-detect-bscan-mask 2;
#targets -set -nocase -filter {name =~ "*microblaze*#0" && bscan=="USER2" };
#rst -system;
#after 3000;
#targets -set -nocase -filter {name =~ "*microblaze*#0" && bscan=="USER2" };
#dow /home/emanuel/Documentos/Vitis/workspace/hello_world/Debug/hello_world.elf;
